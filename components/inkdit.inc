<?php
/**
 * @file
 * Webform module textfield component.
 */

/**
 * Implements _webform_defaults_component().
 */
function _webform_defaults_inkdit() {
  return array(
    'name' => '',
    'form_key' => NULL,
    'pid' => 0,
    'weight' => 0,
    'value' => '',
    'mandatory' => 0,
    'extra' => array(
      'email_field_id' => 'option',
      'unique' => 0,
      'title_display' => 0,
      'attributes' => array(),
      'description' => '',
      'private' => FALSE,
    ),
  );
}

/**
 * Implements _webform_theme_component().
 */
function _webform_theme_inkdit() {
  return array(
    'webform_inkdit_display_inkdit' => array(
      'render element' => 'element',
      'file' => 'components/time.inc',
    ),
  );
}

/**
 * Implements _webform_edit_component().
 */
function _webform_edit_inkdit($component) {
  $form = array();

  $form['value'] = array(
    '#type' => 'textfield',
    '#title' => t('Inkdit Contract ID'),
    '#description' => t('Enter the unique contract id. id:token'),
    '#weight' => 0,
    '#default_value' => $component['value'],
  );

  $node = node_load($component['nid']);

  $email_addresses = array();
  foreach($node->webform['components'] as $field) {
    if($field['type'] == 'email') {
      $email_addresses[$field['form_key']] = $field['name'];
    }
  }

  $form['extra']['email_field_id'] = array(
    '#type' => 'radios',
    '#title' => t('Associated E-mail Component'),
    '#description' => t('Choose an email component from the current webform to send to inkdit.'),
    '#weight' => 2,
    '#options' => $email_addresses,
    '#default_value' => $component['extra']['email_field_id'],
  );

  return $form;
}

/**
 * Implements _webform_render_component().
 */
function _webform_render_inkdit($component, $value = NULL, $filter = TRUE) {
  $element = array(
    '#type' => 'item',
    '#title' => $filter ? _webform_filter_xss($component['name']) : $component['name'],
    '#title_display' => $component['extra']['title_display'] ? $component['extra']['title_display'] : 'before',
    '#translatable' => array('title', 'description', 'field_prefix', 'field_suffix'),
    '#required' => $component['mandatory'],
    '#weight' => $component['weight'],
    '#description' => $filter ? _webform_filter_descriptions($component['extra']['description']) : $component['extra']['description'],
    '#attributes' => $component['extra']['attributes'],
    '#theme_wrappers' => array('webform_element'),
  );

  // Enforce uniqueness.
  if ($component['extra']['unique']) {
    $element['#element_validate'][] = 'webform_validate_unique';
  }

  if (isset($value)) {
    $element['#markup'] = $value[0];
  }

  $contract = explode(":", $component['value']);

  $element['#markup'] = "<div id='inkdit-agree-plugin-container'></div><script src='https://inkdit.com/plugins/agree-plugin.js'></script><script type='text/javascript'>agreeButton = new Inkdit.AgreePluginHelper({'id':'".$contract[0]."','token':'".$contract[1]."','form':'webform-client-form-".$component['nid']."','submitbutton':'op','emailinput':'submitted[".$component['extra']['email_field_id']."]'});</script>";

  return $element;
}

/**
 * Implements _webform_display_component().
 */
function _webform_display_inkdit($component, $value, $format = 'html') {
  return array(
    '#title' => $component['name'],
    '#weight' => $component['weight'],
    '#theme' => 'webform_inkdit_display_inkdit',
    '#theme_wrappers' => $format == 'html' ? array('webform_element') : array('webform_element_text'),
    '#field_prefix' => $component['extra']['field_prefix'],
    '#field_suffix' => $component['extra']['field_suffix'],
    '#format' => $format,
    '#value' => isset($value[0]) ? $value[0] : '',
    '#translatable' => array('title', 'field_prefix', 'field_suffix'),
  );
}

/**
 * Format the output of data for this component.
 */
function theme_webform_inkdit_display_inkdit($variables) {
  $element = $variables['element'];
  $prefix = $element['#format'] == 'html' ? '' : $element['#field_prefix'];
  $suffix = $element['#format'] == 'html' ? '' : $element['#field_suffix'];
  $value = $element['#format'] == 'html' ? check_plain($element['#value']) : $element['#value'];

  return $value !== '' ? ($prefix . $value . $suffix) : ' ';
}

/**
 * Implements _webform_analysis_component().
 */
function _webform_analysis_inkdit($component, $sids = array()) {
  $query = db_select('webform_submitted_data', 'wsd', array('fetch' => PDO::FETCH_ASSOC))
    ->fields('wsd', array('data'))
    ->condition('nid', $component['nid'])
    ->condition('cid', $component['cid']);

  if (count($sids)) {
    $query->condition('sid', $sids, 'IN');
  }

  $nonblanks = 0;
  $submissions = 0;
  $wordcount = 0;

  $result = $query->execute();
  foreach ($result as $data) {
    if (drupal_strlen(trim($data['data'])) > 0) {
      $nonblanks++;
      $wordcount += str_word_count(trim($data['data']));
    }
    $submissions++;
  }

  $rows[0] = array(t('Left Blank'), ($submissions - $nonblanks));
  $rows[1] = array(t('User entered value'), $nonblanks);
  $rows[2] = array(t('Average submission length in words (ex blanks)'), ($nonblanks != 0 ? number_format($wordcount/$nonblanks, 2) : '0'));
  return $rows;
}

/**
 * Implements _webform_table_component().
 */
function _webform_table_inkdit($component, $value) {
  return check_plain(empty($value[0]) ? '' : $value[0]);
}

/**
 * Implements _webform_csv_headers_component().
 */
function _webform_csv_headers_inkdit($component, $export_options) {
  $header = array();
  $header[0] = '';
  $header[1] = '';
  $header[2] = $component['name'];
  return $header;
}

/**
 * Implements _webform_csv_data_component().
 */
function _webform_csv_data_inkdit($component, $export_options, $value) {
  return !isset($value[0]) ? '' : $value[0];
}
